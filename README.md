<h1 align="center">Loop</h1>
<p align="center">
   A Modern Type-Safe Programming Language<br>
   <a href="https://looplang.org/">Website</a> |
   <a href="https://looplang.org/docs">Documentation</a> |
   <a href="https://downloads.looplang.org">Downloads</a> |
   <a href="https://discord.gg/T3tqQBTyJA">Discord</a> |
   <a href="https://looplang.atlassian.net/jira/software/c/projects/LOOP/issues">Jira Board</a><br>
</p>

> **Note:** Loop is still in its early days of development, and thus is not production-ready.

## Development Roadmap

Loop 0.2.0 will be released at the beginning of summer 2022. The main goal is to make sure that everything you want
can be written in Loop; These are the developments we will be working on:

- [ ] Foreign Function Interface to call Rust functions in Loop.
- [ ] Expanding native language features: structs, http, etc.
- [ ] Beginning of standard library
- [ ] Significantly improving documentation

## Get started (usage)

1. Download a pre-built binary from [downloads.looplang.org](https://downloads.looplang.org)
2. Run `./loop` to start the REPL environment or
3. Run `./loop FILENAME.lp` to run a specific file.

## Get Started (development)

1. Make sure you have [Rust](https://www.rust-lang.org/) installed
1. Create a directory to hold all the dependencies `mkdir loop` and enter it `cd loop`
1. Clone the repository `git clone https://gitlab.com/looplanguage/loop`
1. Clone Vinci `git clone https://gitlab.com/looplanguage/vinci`
1. Clone Picasso `git clone https://gitlab.com/looplanguage/picasso`
1. Clone Sanzio `git clone https://gitlab.com/looplanguage/sanzio`   
1. Enter the repository `cd loop`
1. Run the command `cargo run`
1. The Loop shell should now popup

Go to our [Jira](https://looplang.atlassian.net/jira/software/c/projects/LOOP/issues) board to see all the issues and tasks to work on.

To see code documentation, run the command: `cargo doc --open`. A browser tab will open with generated documentation from code comments.

## Guidelines

If you want to contribute to one of the projects, it is recommenced to read two things: [development guidelines](https://gitlab.com/looplanguage/loop/-/wikis/Loop-Language-Development-Guidelines) and the [contributor guidelines](https://looplang.org/contributor_guidelines).

## Code of Conduct

To read our code of conduct, go to the [website](https://looplang.org/conduct).

##

<p align="center">Go to the <a href="https://looplang.org/">website</a> for more information.</p>
