#!/bin/bash
if ! command -v cargo &> /dev/null
then
    echo "Cargo could not be found!"
    exit 1
fi

move_to_prod() {
  mv Cargo.toml Cargo-dev.toml
  mv Cargo-production.toml Cargo.toml
}

move_to_dev() {
  mv Cargo.toml Cargo-production.toml
  mv Cargo-dev.toml Cargo.toml
}


if [ "$1" == "dev" ]
then
  echo "Building development build..."
  cargo build
elif [ "$1" == "prod" ]
then
  echo "Building production build..."
  move_to_prod
  cargo build --release || move_to_dev
  move_to_dev
elif [ "$1" == "cilint" ]
then
  echo "Linting CI..."
  move_to_prod
  cargo clippy -- -D warnings || move_to_dev
  cargo fmt --all -- --check || move_to_dev
  move_to_dev
elif [ "$1" == "ci" ]
then
  echo "Running CI tests..."
  move_to_prod
  cargo test || move_to_dev
  move_to_dev
elif [ "$1" == "test" ]
then
  echo "Running tests..."
  cargo test
else
  echo "Unknown command \"$1\", expected: dev, prod, test"
  exit 1
fi